         ######   ####   #####   #####    ###  ###
         ##  ##  ##     ##      ##   ##  ##  ##  ##
         ##  ##  #####  ##      #######  ##  ##  ##
         ##  ##     ##  ##      ##   ##  ##      ##
         ######  ####    #####  ##   ##  ##      ## R
################################################################
# Title:.......Automatic install/update OScam                  #
# Author:......Prilly                                          #
# Support:.....prilly@speedbox.me                              #
# Date:........06 September 2015                               #
# Description:.Automaticaly install OScam with systemd support #
#              Updates OScam to latest or specific version     #
################################################################
It is just cc of original svn repo
(http://svn.speedbox.me/svn/oscam-install/trunk).

################################################################

Automatic install script for Multi instance OScam deployments
it will install 1 to 9 instances of OScam depending on selection

To run this script you need to be root users:
#################################################################

apt-get -y install subversion dialog git
git clone https://gitlab.com/atashka1995/oscam.git
chmod -R 0755 oscam
cd oscam
./install.sh

#################################################################

IMPORTANT: Depends on Systemd, SysV is now legacy!

1. It will compile OScam from Streamboard for HEAD or desired SVN version. 
   both for upgrade and new installations

2. Install script will also install FIRMWARE for HID omnikey usb readers 
   (if selected) this firmware is downloaded from HID global, 
   http://www.hidglobal.com/drivers it will also 
   setup dependency's for smartreader (smargo usb) (if selected)

3. systemd startup script for OScam is also installed, this makes monitoring 
   and default startup of OScam upon restart of server easy, it also support 
   status of running OScam deamon.

4. properly set up logging for each instances of oscam /var/log/oscam/oscam[1-9]

5. properly setting up config directory AND install optimized default 
   config to avoid oscam instances to use same ports /usr/local/etc/oscam[1-9]

6. Install script also contains a upgrade function to upgrade ALL 
   instances of oscam to desired OR HEAD version ALWAYS use this to 
   change version of the running oscam binary (ALL instances of oscam 
   is running same SVN version, its not possible to have different SVN 
   versions of oscam for different instances) 
   WHEN RUNNING UPGRADE OPTION NO CONFIG FILES ARE TOUCHED, 
   THIS PRESERV USER CHANGES IN OSCAM


Info:
################################################################
OScam binary is placed here: /usr/local/bin/oscam{INSTANCE Number}
OScam temp directory: /tmp/.oscam{INSTANCE Number}/
OScam PID is placed here: /var/run/oscam{INSTANCE Number}.pid
OScam log directory: /var/log/oscam/oscam{INSTANCE Number}/
OScam Config directory: /usr/local/etc/oscam{INSTANCE Number}/
OScam Systemd scripts: /etc/systemd/system/oscam{INSTANCE Number}.service

URL: http://your-ip:888(1->9)
Login: root/root
################################################################

To start or stop OScam you can use this commands, or you can use the restart 
inside OScam Webif:

################################################################

systemctl start oscam{INSTANCE Number}.service		# Start
systemctl stop oscam{INSTANCE Number}.service		# Stop
systemctl restart oscam{INSTANCE Number}.service	# Restart
systemctl status oscam{INSTANCE Number}.service		# Status

################################################################

If you have used this script and want to update to a 
later OR SPECIFIC svn version of oscam use update selection

This script will prompt you with a menu asking if you want to 
update to latest svn version of oscam or if you want to update 
to a specific version, 
if you choose specific enter the svn version number and hit enter.

Script will then replace current OScam binary with new one 

See header for mail support for this script !

Regards
Prilly